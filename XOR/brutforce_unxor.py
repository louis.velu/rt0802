#python3
#louis vélu


message_chiffre = bytearray("azertyuiop", "utf8")

#charge le disctionnaire dans une hashtable
dico={}
file_dico = open('dico.txt','r')
for line in file_dico:
	for word in line.split():
		dico["word"]=True

#clé de départ
key=[0]*len(message_chiffre)


while True:

	result = ""
	for e in range(len(message_chiffre)):
		result += chr(message_chiffre[e] ^ key[e])

	result_words = result.split()
	nb_words=0
	#nombre de mots dans le dictionaire ?
	for word in result_words:
		if word in dico:
			nb_words+=1

	if nb_words/len(result_words)*100 > 25:
		print("------------------------")
		print("Resultat : "+result)
		key_string = ''.join(chr(i) for i in key)
		print("Clée : "+key_string)
		print("Resultat (Hex) : "+result.encode('utf8').hex())
		print("Clée (Hex) : "+key_string.encode('utf8').hex())
	#incrémente la clé pour le prochain tour
	i=0
	key[0]+=1
	while key[i] > 254:
		key[i]=0
		i+=1
		key[i]+=1

	if key[len(key)-1] > 254: #si on a vue toutes les clées
		break
