#python 3
#louis vélu

import binascii

mon_message = binascii.unhexlify("0322385e15422b")
#mon_message = bytearray('bonjour', 'ascii')
#key = bytearray("aMV4z7Yrqu4n8eUKUsprLSjwwQYt8S3VDyM8ZGV8vGutFBjbNRG7HZdpze8d5xMUmYkPjZxgBhT9Km3e2aJ9L36QKNAG7txDGBLqHZUNwAeqWuqpzDhDx3JHYZ3QxE5nChPfA5HWjwEVsKfZKkaTWX2vHYbY4ytCtSMDb7JfBHamuRGZT7yZpuX8FEpDbtuggxr9YKcPe82zZvqNQX3NqMa9S92eUT6sXENwPERNE5ZVT2NT57juP6GkgQg5xC35", "ascii")
key = binascii.unhexlify("774d4c31612d5f")
result = ""
for e in range(len(mon_message)):
	result+=chr(mon_message[e] ^ key[e])
print(result)
print(result.encode('utf8').hex())
