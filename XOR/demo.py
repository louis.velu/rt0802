#python 3
#louis vélu

import binascii
import ctypes
#print(chr(binascii.unhexlify('03')[0]))

mon_message = bytearray('toto', 'utf8')
#crypted_message = bytearray('azertyuiop', 'ascii')
crypted_message = binascii.unhexlify("0322385e15422b")
result = ""

for e in range(len(crypted_message)):
	result+=chr(ctypes.c_ubyte((crypted_message[e] ^ mon_message[e % len(mon_message)])).value)

print(result.encode('utf8').hex())
print(result)
